﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltaUsuarios.aspx.cs" Inherits="PracticaOEE.AltaUsuarios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 82%;
        }
        .auto-style3 {
            text-align: left;
            width: 210px;
        }
        .auto-style4 {
            width: 210px;
            height: 23px;
        }
        .auto-style6 {
            text-align: left;
            width: 210px;
            height: 9px;
        }
        .auto-style7 {
            height: 9px;
        }
        .auto-style8 {
            width: 210px;
            height: 32px;
            text-align: left;
        }
        .auto-style9 {
            text-align: right;
            width: 224px;
            height: 23px;
        }
        .auto-style11 {
            width: 210px;
            height: 23px;
            text-align: left;
        }
        .auto-style12 {
            margin-left: 0px;
        }
        .auto-style14 {
            height: 32px;
        }
        .auto-style16 {
            margin-left: 0px;
            text-align: right;
            width: 224px;
        }
        .auto-style17 {
            margin-left: 0px;
            text-align: center;
            height: 36px;
        }
        .auto-style18 {
            font-size: x-large;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table align="center">
                <tr>
                    <td class="auto-style17" colspan="3"><strong><span class="auto-style18">ALTA DE USUARIOS</span></strong></td>
                </tr>
                <tr>
                    <td class="auto-style16">Nombre:&nbsp; </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="TextBoxNombre" runat="server" Width="200px" Height="20px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxNombre" ErrorMessage="El Nombre es obligario" ForeColor="Red" ValidationGroup="EnviarDatos"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style16">Edad:</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="TextBoxEdad" runat="server" Width="200px" Height="20px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxEdad" ErrorMessage="La Edad es obligatoria" ForeColor="Red" ValidationGroup="EnviarDatos"></asp:RequiredFieldValidator>
                        <br />
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxEdad" ErrorMessage="Edad no admitida" ForeColor="Red" MaximumValue="255" MinimumValue="1" ValidationGroup="EnviarDatos" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style16">Email:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TextBoxEmail" runat="server" Width="200px" Height="20px"></asp:TextBox>
                    </td>
                    <td class="auto-style7">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="El Email es obligatorio" ForeColor="Red" ValidationGroup="EnviarDatos"></asp:RequiredFieldValidator>
                        <br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="El email introducido no es válido" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="EnviarDatos"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style16">Contraseña:</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="TextBoxContrasena" runat="server" TextMode="Password" Width="200px" Height="20px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxContrasena" ErrorMessage="La Contraseña es obligatoria" ForeColor="Red" ValidationGroup="EnviarDatos"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style16">Tipo de alta:</td>
                    <td class="auto-style8">
                        <asp:DropDownList ID="DropDownListTipoAlta" runat="server" Height="20px" Width="208px">
                            <asp:ListItem>Persistente</asp:ListItem>
                            <asp:ListItem>Volátil</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style14"></td>
                </tr>
                <tr>
                    <td class="auto-style9"></td>
                    <td class="auto-style11">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Enviar Datos" Height="34px" Width="95px" ValidationGroup="EnviarDatos" />
                    &nbsp;&nbsp;
                        <asp:Button ID="Button2" runat="server" CssClass="auto-style12" Height="34px" OnClick="Button2_Click" Text="Volver" Width="95px" />
                    </td>
                    <td class="auto-style4">
                        &nbsp;</td>
                </tr>
            </table>
        </div>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
