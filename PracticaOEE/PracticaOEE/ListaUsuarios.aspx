﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListaUsuarios.aspx.cs" Inherits="PracticaOEE.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style4 {
            margin-top: 0px;
        }
        .auto-style5 {
            width: 100%;
        }
        .auto-style6 {
            font-size: x-large;
        }
        .auto-style7 {
            height: 26px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
            <table class="auto-style5">
                <tr>
                    <td align = "RIGHT" class="auto-style7">
                        &nbsp;&nbsp;
                        &nbsp;
                        <asp:Label ID="Label1" runat="server" Text="Bienvenido,  " Height="25px"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Cerrar Sesión" Height="25px" />
                    </td>
                </tr>
                <tr>
                    <td align = "CENTER" class="auto-style6">
                        <strong>LISTA DE USUARIOS</strong></td>
                </tr>
                <tr>
                    <td align="CENTER">
                        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourceBBDDpracticaOEE" ForeColor="#333333" GridLines="None" Height="98px" Width="581px" CssClass="auto-style4">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                <asp:BoundField DataField="Edad" HeaderText="Edad" SortExpression="Edad" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSourceBBDDpracticaOEE" runat="server" ConnectionString="<%$ ConnectionStrings:UsuariosConnectionString %>" SelectCommand="SELECT [Nombre], [Edad], [Email] FROM [TableUsuarios]"></asp:SqlDataSource>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
