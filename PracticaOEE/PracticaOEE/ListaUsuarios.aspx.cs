﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PracticaOEE
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("Autenticacion.aspx");
            }
            else
            {
                Label1.Text += Session["Nombre"].ToString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["Email"] = null;
            Session["Nombre"] = null;
            Response.Redirect("Autenticacion.aspx");
        }
    }
}