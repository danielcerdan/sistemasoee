﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Autenticacion.aspx.cs" Inherits="PracticaOEE.Autenticacion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style6 {
            height: 42px;
        }
        .auto-style8 {
            text-align: center;
            font-size: x-large;
            height: 44px;
        }
        .auto-style9 {
            text-align: right;
            width: 219px;
        }
        .auto-style10 {
            text-align: right;
            width: 308px;
        }
        .auto-style11 {
            width: 308px;
        }
        .auto-style12 {
            text-align: right;
            height: 26px;
        }
        .auto-style13 {
            height: 26px;
        }
        .auto-style14 {
            height: 26px;
            width: 219px;
        }
        .auto-style15 {
            width: 219px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table  align = "center">
                <tr>
                    <td></td>
                    <td class="auto-style8" colspan="2"><strong>AUTENTIFICACIÓN</strong></td>
                </tr>
                <tr>
                    <td class="auto-style12">Email: </td>
                    <td align="center" class="auto-style14" colspan="2">
                        <asp:TextBox ID="TextBoxEmail" runat="server" Width="200px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Por favor, introduzca Email" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style12">Contraseña: </td>
                    <td align="center" class="auto-style14" colspan="2">
                        <asp:TextBox ID="TextBoxContrasena" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxContrasena" ErrorMessage="Por favor, introduzca Contraseña" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" class="auto-style9" colspan="2">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Enviar" Height="34px" Width="78px" />
                    </td>
                    <td class="auto-style6">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/AltaUsuarios.aspx">Registrar nuevo usuario</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
