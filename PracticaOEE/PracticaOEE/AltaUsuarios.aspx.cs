﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace PracticaOEE
{
    public partial class AltaUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["UsuariosConnectionString"].ConnectionString);
                    connect.Open();

                    string checkEmail = "select count(*) from TableUsuarios where Email='" + TextBoxEmail.Text + "'";
                    SqlCommand comando = new SqlCommand(checkEmail, connect);
                    int temp = Convert.ToInt32(comando.ExecuteScalar().ToString());
                    if(temp==1)
                    {
                        Response.Write("El Email ya existe en la base de datos");
                    }

                    connect.Close();
                }
                catch (Exception ex)
                {
                    Response.Write("Error: " + ex.ToString());
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (DropDownListTipoAlta.SelectedItem.ToString() == "Persistente")
            {
                try
                {
                    SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["UsuariosConnectionString"].ConnectionString);
                    connect.Open();
                    string checkEmail = "select count(*) from TableUsuarios where Email='" + TextBoxEmail.Text + "'";
                    SqlCommand comando = new SqlCommand(checkEmail, connect);
                    int temp = Convert.ToInt32(comando.ExecuteScalar().ToString());
                    connect.Close();
                    if (temp != 1)
                    {
                        connect = new SqlConnection(ConfigurationManager.ConnectionStrings["UsuariosConnectionString"].ConnectionString);
                        connect.Open();

                        string insertarUsuario = "insert into TableUsuarios (Nombre, Edad, Email, Contrasena) values (@nombre, @edad, @email, @contrasena)";
                        comando = new SqlCommand(insertarUsuario, connect);
                        comando.Parameters.AddWithValue("@nombre", TextBoxNombre.Text);
                        comando.Parameters.AddWithValue("@edad", TextBoxEdad.Text);
                        comando.Parameters.AddWithValue("@email", TextBoxEmail.Text);
                        comando.Parameters.AddWithValue("@contrasena", TextBoxContrasena.Text);
                        comando.ExecuteNonQuery();
                        Session["Nombre"] = TextBoxNombre.Text;
                        Session["Email"] = TextBoxEmail.Text;
                        Response.Redirect("ListaUsuarios.aspx");

                        connect.Close();
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("Error: " + ex.ToString());
                }
            }
            else
            {
                Session["Nombre"] = TextBoxNombre.Text;
                Session["Email"] = TextBoxEmail.Text;
                Response.Redirect("ListaUsuarios.aspx");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Autenticacion.aspx");
        }
    }
}